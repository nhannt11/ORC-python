import os
import cv2
import numpy as np
from PyQt5 import QtCore, QtGui, QtWidgets                     # uic
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QWidget, 
                             QLabel, QVBoxLayout,QFileDialog)              # +++

from test import Ui_Form
from test import App
from test import UIWindow
from PIL import Image, ImageOps
from gtts import gTTS
import pytesseract
from pytesseract import image_to_string
from playsound import playsound                                 # +++
#import time
import re
from pytesseract import Output
from imutils.object_detection import non_max_suppression



class video (QtWidgets.QDialog, Ui_Form):

    
    def __init__(self):
        super().__init__()


        self.setupUi(self)                         
        self.control_bt.clicked.connect(lambda: self.start_webcam())

        self.capture.clicked.connect(lambda: self.capture_image())
        #self.capture.clicked.connect(lambda: self.startUIWindow("turn on camera first"))
        self.capture.clicked.connect(lambda: self.read())


        self.choose_bt.clicked.connect(lambda: self.startUIWindow("language= "+ self.lang1 +"\n mode: "+self.mode + "\n rotate " + str(self.slider.value())+ " clockwise"))
        self.choose_bt.clicked.connect(self.conv)

        self.trans_bt.clicked.connect(lambda: self.startUIWindow("language= "+ self.lang1 +"\n mode: "+self.mode + "\n rotate " + str(self.slider.value())+ " clockwise"))
        self.trans_bt.clicked.connect(self.trans)

        
        self.lang1_bt.pressed.connect(lambda: self.set_lang(1))
        self.lang2_bt.pressed.connect(lambda: self.set_lang(2))

        #self.mode1_bt.pressed.connect(lambda: self.set_mode(1))
        self.mode2_bt.pressed.connect(lambda: self.set_mode(2))
        self.mode3_bt.pressed.connect(lambda: self.set_mode(3))
       # - ()

        self.image_label.setScaledContents(True)

                                              #  -capture <-> +cap

        self.timer = QtCore.QTimer(self, interval=5)
        self.timer.timeout.connect(self.update_frame)

        
   
    @QtCore.pyqtSlot()
    def start_webcam(self):
        #self.cap = None
        if True:
            self.cap = cv2.VideoCapture(0,cv2.CAP_DSHOW)
            self.cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 480)
            self.cap.set(cv2.CAP_PROP_FRAME_WIDTH,  640)
        self.timer.start()

    
    @QtCore.pyqtSlot()
    def update_frame(self):
        flag, image = self.cap.read()
        #image     = cv2.flip(image, 1)
        if flag == True :
            self.displayImage(image, True)

    @QtCore.pyqtSlot()
    def capture_image(self):
        if self.cap is not None:
            flag, frame = self.cap.read()
            #path = r'/home/pi/luan_van/'                               #duong dan dung tren pi3
            path = r'C:\Users\User\Desktop\learnPyQT\data'              # duong dan dung tren window
            if flag:
                QtWidgets.QApplication.beep()
                name = "0.jpg"
                cv2.imwrite(os.path.join(path, name), frame)
            self.cap.release()
            cv2.destroyAllWindows()
        else:
            self.startUIWindow("turn on camera first")

    
    @QtCore.pyqtSlot()
    def set_lang(self,lan):
        if lan == 1:
            self.lang1='eng'
            self.lang2='en'
        if lan == 2:
            self.lang1='vie'
            self.lang2='vi'
      

    @QtCore.pyqtSlot()
    def read(self):
        if self.cap is not None:
        	self.ad ='C:\\Users\\User\\Desktop\\learnPyQT\\data\\0.jpg'  #duong dan dung tren window
        	#self.ad ='/home/pi/luan_van/0.jpg'
        	#img= Image.open('C:\\Users\\User\\Desktop\\learnPyQT\\data\\0.jpg')
        	#img.show()
            #self.trans('/home/pi/luan_van/0.jpg')  

            #self.trans('C:\\Users\\User\\Desktop\\learnPyQT\\data\\0.jpg')   #duong dan dung tren window
            #self.trans('/home/pi/luan_van/0.jpg')                           #duong dan dung tren pi3
        

    @QtCore.pyqtSlot()
    def conv (self):
        ex = App()
        a = App.openFileNameDialog(App())
        self.ad = a
        if not a=='':
        	#img= Image.open(a)
        	#img.show()
        	img = cv2.imread(a)
        	dim = (800,600)
        	img = cv2.resize(img, dim,interpolation=cv2.INTER_NEAREST)
        	self.displayImage(img, True)
        self.goWindow1()
        #self.trans(a)
       

    def set_mode(self, state):

        if state ==2:
            self.mode= "document"
        else :
            self.mode= "real life image"

    def deskew(self,image):
            angle=-self.slider.value()
            height, width = image.shape[:2] 
            image_center = (width/2, height/2)
            rotation_mat = cv2.getRotationMatrix2D(image_center, angle, 1.)
            abs_cos = abs(rotation_mat[0,0])
            abs_sin = abs(rotation_mat[0,1])
            bound_w = int(height * abs_sin + width * abs_cos)
            bound_h = int(height * abs_cos + width * abs_sin)
            rotation_mat[0, 2] += bound_w/2 - image_center[0]
            rotation_mat[1, 2] += bound_h/2 - image_center[1]
            rotated_mat = cv2.warpAffine(image, rotation_mat, (bound_w, bound_h))
            return rotated_mat
    def change_contrast(self,img, level):
        factor = (259 * (level + 255)) / (255 * (259 - level))
        def contrast(c):
            value= 128 + factor * (c - 128)
            return max(0,min(255,value))
        return img.point(contrast)
    def decode_predictions(self,scores, geometry):
        (numRows, numCols) = scores.shape[2:4]
        rects = []
        confidences = []
        for y in range(0, numRows):
            scoresData = scores[0, 0, y]
            xData0 = geometry[0, 0, y]
            xData1 = geometry[0, 1, y]
            xData2 = geometry[0, 2, y]
            xData3 = geometry[0, 3, y]
            anglesData = geometry[0, 4, y]
            for x in range(0, numCols):
                if scoresData[x] < 0.5:
                    continue
                (offsetX, offsetY) = (x * 4.0, y * 4.0)
                angle = anglesData[x]
                cos = np.cos(angle)
                sin = np.sin(angle)
                h = xData0[x] + xData2[x]
                w = xData1[x] + xData3[x]
                endX = int(offsetX + (cos * xData1[x]) + (sin * xData2[x]))
                endY = int(offsetY - (sin * xData1[x]) + (cos * xData2[x]))
                startX = int(endX - w)
                startY = int(endY - h)
                rects.append((startX, startY, endX, endY))
                confidences.append(scoresData[x])
        return (rects, confidences)
    
    def trans(self,ad):
        if (self.ad) =="":
            self.goWindow1()
        else:
            if self.mode == "real life image":
                image = cv2.imread(self.ad)
                #cv2.imshow("origin",image)
                orig = image.copy()
                (origH, origW) = image.shape[:2]
                (newW, newH) = (320,320)
                rW = origW / float(newW)
                rH = origH / float(newH)
                image = cv2.resize(image, (newW, newH))
                (H, W) = image.shape[:2]
                layerNames = ["feature_fusion/Conv_7/Sigmoid","feature_fusion/concat_3"]
                net = cv2.dnn.readNet('frozen_east_text_detection.pb')
                blob = cv2.dnn.blobFromImage(image, 1.0, (W, H),(123.68, 116.78, 103.94), swapRB=True, crop=False)
                net.setInput(blob)
                (scores, geometry) = net.forward(layerNames)
                (rects, confidences) = self.decode_predictions(scores, geometry)
                boxes = non_max_suppression(np.array(rects), probs=confidences)
                results = []
                textend=""
                for (startX, startY, endX, endY) in boxes:
                    startX = int(startX * rW)
                    startY = int(startY * rH)
                    endX = int(endX * rW)
                    endY = int(endY * rH)
                    dX = int((endX - startX) * 0.5)
                    dY = int((endY - startY) * 0.5)
                    startX = max(0, startX - dX)
                    startY = max(0, startY - dY)
                    endX = min(origW, endX + (dX * 2))
                    endY = min(origH, endY + (dY * 2))
                    roi = orig[startY:endY, startX:endX]
                    custom_config= r'--oem 1 --psm 7'
                    text = pytesseract.image_to_string(roi,lang= 'eng', config=custom_config)
                    results.append(((startX, startY, endX, endY), text))
                results = sorted(results, key=lambda r:r[0][1])
                output = orig.copy()
                endtext= ""
                for ((startX, startY, endX, endY), text) in results:
                    text = "".join([c if ord(c) < 128 else "" for c in text]).strip()
                    endtext = endtext +"--"+ text
                    output = orig.copy()
                    cv2.rectangle(output, (startX, startY), (endX, endY),
                        (0, 0, 255), 2)
                    cv2.putText(output, text, (startX, startY - 20),cv2.FONT_HERSHEY_SIMPLEX, 1.2, (0, 0, 255), 3)
                    cv2.imshow("Text Detection", output)
                    cv2.waitKey(0)
                #cv2.imshow("origin image",image)
                self.startUIWindow(endtext)
                
            else: 
                if self.mode == "document" :
                	img = cv2.imread(self.ad)
                	im = Image.open(self.ad)
                	im.show()
                	img = cv2.fastNlMeansDenoisingColored(img, None, 10, 10, 7, 15) 
                	re_img1 = self.deskew(img)
                	re_img1 = cv2.cvtColor(re_img1, cv2.COLOR_BGR2GRAY)
                	re_img1 = cv2.adaptiveThreshold(re_img1,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,cv2.THRESH_BINARY,21,9)
                	kernel = np.ones((3, 3), np.uint8)
                	kernel2 = np.ones((2, 2), np.uint8)
                	re_img1 = cv2.erode(re_img1, kernel)
                	re_img1 = cv2.dilate(re_img1, kernel2, iterations=1)
                	#(h, w) = img.shape[:2]
                	#r = 640 / float(h)
                	#dim = (640, int(h * r))
                	#dim = (int(w * r),640)
                	# img1 = cv2.resize(re_img1, dim,interpolation=cv2.INTER_NEAREST)
                	#cv2.imshow('open', img1)
                	cv2.imwrite('C:\\Users\\User\\Desktop\\learnPyQT\\data\\3.jpg', re_img1)
                	im2=Image.open('C:\\Users\\User\\Desktop\\learnPyQT\\data\\3.jpg')
                	#cv2.imwrite('/home/pi/luan_van/1.jpg',re_img1)
                	#im2=Image.open('/home/pi/luan_van/1.jpg')
                	im2.show()
                	#org = cv2.resize(img, dim,interpolation=cv2.INTER_NEAREST)
                	#cv2.imshow("origin", org)
             
                custom_config = r'--oem 3 --psm 6'
                decoded_text = image_to_string(re_img1,lang= self.lang1,config=custom_config)
                cleaned_text="".join(c for c in decoded_text if (c.isalnum() or c==' ' or c=='\n'))
                self.startUIWindow(decoded_text)
               
                # if decoded_text.strip():
                #     tts = gTTS(cleaned_text, lang= self.lang2)
                #     if os.path.exists('speech.mp3'):
                #         os.remove("speech.mp3")         
                #     tts.save("speech.mp3")
                #     playsound("speech.mp3")                         #window
                #     #os.system('omxplayer -o alsa speech.mp3 &')    #pi3
                # else:
                #     self.startUIWindow("Cant find text")
                if not decoded_text.strip():
                	self.startUIWindow("Cant find text")
                   

    def displayImage(self, img, window=True):
        qformat = QtGui.QImage.Format_Indexed8
        if len(img.shape)==3 :
            if img.shape[2]==4:
                qformat = QtGui.QImage.Format_RGBA8888
            else:
                qformat = QtGui.QImage.Format_RGB888
        outImage = QtGui.QImage(img, img.shape[1], img.shape[0], img.strides[0], qformat)
        outImage = outImage.rgbSwapped()
        if window:
            self.image_label.setPixmap(QtGui.QPixmap.fromImage(outImage))

    def startUIWindow(self,end_text):
        # if self.cap is not None:
        #     self.cap.release()
        #     cv2.destroyAllWindows()
        self.Window = UIWindow()                               # - self
        self.setWindowTitle("UIWindow")
        self.Window.label.setText(end_text)
        #self.Window.label.setFont(QtGui.QFont('Arial', 30))
        self.Window.label.setWordWrap(True)
        self.Window.label.repaint()
        self.Window.ToolsBTN.clicked.connect(self.goWindow1)
        self.hide()
        self.Window.show()
    def goWindow1(self):
        cv2.destroyAllWindows()
        self.show()
        self.Window.hide()

        

if __name__=='__main__':
    import sys
    app = QtWidgets.QApplication(sys.argv)
    window = video()
    window.setWindowTitle('OCR on Raspberry pi')
    window.show()
   
    sys.exit(app.exec_())