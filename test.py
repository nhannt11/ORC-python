from PyQt5 import QtCore, QtGui, QtWidgets                   # uic
from PyQt5.QtWidgets import (QApplication, QMainWindow, QPushButton, QWidget, 
                             QLabel, QVBoxLayout,QFileDialog,QFrame,QGridLayout,QSlider) 


class Ui_Form(object):
    def changeValue(self, value):
        self.label.setText(str(value))
        

    def setupUi(self, Form):
        Form.setObjectName("Form")
        Form.resize(1050, 700)
        self.horizontalLayout = QtWidgets.QHBoxLayout(Form)
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.w = QWidget()
        self.w.setWindowTitle("QButtonGrounp")
        self.w.resize(80, 250)


        self.lang1='eng'
        self.lang2='en'
        self.mode ="document"
        self.cap = None
        self.ad=''
        
            

        
        self.lang1_bt = QtWidgets.QRadioButton(f"english",self.w)
        self.lang1_bt.setChecked(True)
        self.lang1_bt.move(0, 0)
        
        self.lang2_bt = QtWidgets.QRadioButton(f"vietnamese",self.w)
        self.lang2_bt.move(0, 20)


        self.lang_group=QtWidgets.QButtonGroup(self.w)
        self.lang_group.addButton(self.lang1_bt)
        self.lang_group.addButton(self.lang2_bt)

        self.mode_group=QtWidgets.QButtonGroup(self.w)
        #self.mode1_bt = QtWidgets.QRadioButton(f"clean document",self.w)
        #self.mode1_bt.move(0, 80)
        #self.mode1_bt.setChecked(True) 
        self.mode2_bt = QtWidgets.QRadioButton(f"document",self.w)
        self.mode2_bt.move(0,70)
        self.mode2_bt.setChecked(True)
        self.mode3_bt = QtWidgets.QRadioButton(f"real life image",self.w)
        self.mode3_bt.move(0,90)

        self.slider = QtWidgets.QSlider(QtCore.Qt.Horizontal, self.w)
        self.slider.move(0,130)
        self.slider.setRange(-180, 180)
        self.slider.setPageStep(5)
        self.slider.valueChanged[int].connect(self.changeValue)
        #self.slider.valueChanged.connect(self.updateLabel)
        self.label = QtWidgets.QLabel('0', self.w)
        self.label.setGeometry(QtCore.QRect(100, 125, 150, 30))
        
        
            
        #self.slider.setFocusPolicy(QtCore.Qt.NoFocus)

    
        

        self.image_label = QtWidgets.QLabel(Form)
        self.image_label.setText("select language and mode")
        self.image_label.setObjectName("image_label")
        self.verticalLayout.addWidget(self.image_label)


        self.control_bt = QtWidgets.QPushButton(self.w)
        self.control_bt.setGeometry(QtCore.QRect(0, 180, 150, 30))
        

        self.capture = QtWidgets.QPushButton(self.w)
        self.capture.setGeometry(QtCore.QRect(0, 220, 150, 30))
       

        self.choose_bt = QtWidgets.QPushButton(self.w)
        self.choose_bt.setGeometry(QtCore.QRect(0, 260, 150, 30))

        self.trans_bt = QtWidgets.QPushButton(self.w)
        self.trans_bt.setGeometry(QtCore.QRect(0, 300, 150, 30))
        
        
        self.horizontalLayout.addWidget(self.w)
        self.horizontalLayout.addLayout(self.verticalLayout)

        self.retranslateUi(Form)
        QtCore.QMetaObject.connectSlotsByName(Form)

    def retranslateUi(self, Form):
        _translate = QtCore.QCoreApplication.translate
        Form.setWindowTitle(_translate("Form",     "Cam view"))
        self.control_bt.setText(_translate("Form", "Start Camera"))
        self.capture.setText(_translate("Form",    "Capture"))
        self.choose_bt.setText(_translate("Form",    "Browse"))
        self.trans_bt.setText(_translate("Form",    "Get Text"))

class App(QWidget):

    def __init__(self):
        super().__init__()
        self.title = 'PyQt5 file dialogs - pythonspot.com'
        self.left = 10
        self.top = 10
        self.width = 640
        self.height = 480
        self.initUI()
    
    def initUI(self):
        self.setWindowTitle(self.title)
        self.setGeometry(self.left, self.top, self.width, self.height)
        
        
    def openFileNameDialog(self):
        options = QFileDialog.Options()
        options |= QFileDialog.DontUseNativeDialog
        fileName, _ = QFileDialog.getOpenFileName(self,"QFileDialog.getOpenFileName()", "","All Files (*);;Python Files (*.py)", options=options)
        return fileName

class UIWindow(QWidget):
    def __init__(self, parent=None):
        super(UIWindow, self).__init__(parent)

        self.resize(640, 640)
        
        self.label = QLabel("Dang chay chuong trinh", alignment=QtCore.Qt.AlignCenter)
        self.label.setFont(QtGui.QFont('Arial', 20))

        self.scrollarea = QtWidgets.QScrollArea(self)
        self.scrollarea.setFixedSize(640, 600)
        self.scrollarea.setWidgetResizable(True)
        self.scrollarea.setWidget(self.label)
        
        # 
        
        #self.layout_groupbox.addStretch(1)


        self.ToolsBTN = QPushButton('back')
        self.v_box = QVBoxLayout()
        #self.v_box.addWidget(self.label)
        self.v_box.addWidget(self.scrollarea)
        self.v_box.addWidget(self.ToolsBTN)
        #self.scrollarea.setLayout(self.v_box)
        self.setLayout(self.v_box)
        self.show()

 